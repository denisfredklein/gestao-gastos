# GESTÃO DE GASTOS API #

O objetivo desta api é fornecer microsserviços de gestão de gastos conforme especificado no exercício proposto

#################  Massa de testes #################

A aplicação utiliza um banco de dados H2 (em memória). Ao subir a aplicação é lido um script que carrega automaticamente alguns dados para uso:

# Usuários #

A aplicação possui dois usuários que são incluídos automaticamente quando a mesma sobe, um de login CLIENTE e outro de login SISTEMA.
    1 - login: SISTEMA, senha: 123 (código de usuário 1)
    2 - login: CLIENTE, senha: 123 (código de usuário 2)

O usuário do tipo sistema foi criado para atender ao requisito que dizia que "Apenas sistemas credenciados poderão incluir novos gastos"

O usuário CLIENTE já possui 5 gastos atribuídos a ele.

# Categorias #

Algumas categorias são cadastradas automaticamente. Como não fazia parte dos requisitos, não foi incluído um serviço de inclusão de categorias, ficando neste momento restrito ao uso destes.

1 - Bares e restaurantes (ID = 1)
2 - Mercado (ID = 2)
3 - Jóias (ID = 3)

Dois dos gastos atribuídos ao usuário CLIENTE possuem uma categoria cada.

################# API #################

# Obter token #

A API precisa de atenticação por token. O mesmo deve ser atachado ao header de todas as requisições com a chave "Authorization" (exceto para a requisição de obtenção do token).

O token pode ser obtido na resposta da requisição:

    http://localhost:8080/gestaogastos/autenticar

A mesma deve ser feita com o método POST, enviando um objeto JSON no formato:

{
    "login":"CLIENTE",
	"senha":"123"
}

# Integração de gastos por cartão #

Inclui um gasto. Só pode ser incluído para usuário do tipo CLIENTE (não sento possível atribuir um gasto a um sistema)

URL:        http://localhost:8080/gestaogastos/gasto/add
Método:     POST
Headers:    Content-Type = application/json
            Authorization = (token obtido)
Corpo:      {
            	"descricao": "descrição exemplo",
            	"valor": 99.99,
            	"codigousuario": 2,
            	"data": "2012-04-23T18:25:43.511Z"
            }
# Categorização automatica de gasto #

Não se trata de um serviço, mas de um requisito incorporado à integração de gastos por cartão: foi implementada a funcionalidade de inclusão automática de categorias quando um gasto inserido possui o mesmo nome de um gasto inserido anteriormente.

# Listagem de gastos #

Lista todos os gastos de um cliente, dados seu login e senha.

URL:        http://localhost:8080/gestaogastos/gasto/listar
Método:     POST
Headers:    Content-Type = application/json
            Authorization = (token obtido)
Corpo:      {
            	"login":"CLIENTE",
            	"senha":"123"
            }

# Filtro de gastos #

Lista todos os gastos de um cliente de um dia informado através da URL:

URL:        http://localhost:8080/gestaogastos/gasto/listar/{ano}/{mes}/{dia}
Método:     POST
Headers:    Content-Type = application/json
            Authorization = (token obtido)
Corpo:      {
            	"login":"CLIENTE",
            	"senha":"123"
            }

# Categorização de gastos #

Permite atribuir a um usuário do tipo CLIENTE adicionar categorias a um gasto, desde que trate-se de uma categoria já cadastrada no sistema e que o cliente informe login e senha no corpo da requisição.

URL:        http://localhost:8080/gestaogastos/gasto/categoria/{id do gasto}/{id da categoria}
Método:     POST
Headers:    Content-Type = application/json
            Authorization = (token obtido)
Corpo:      {
            	"login":"CLIENTE",
            	"senha":"123"
            }
