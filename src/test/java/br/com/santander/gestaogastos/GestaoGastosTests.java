package br.com.santander.gestaogastos;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.santander.gestaogastos.model.Categoria;
import br.com.santander.gestaogastos.model.Gasto;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.model.TipoPessoaEnum;
import br.com.santander.gestaogastos.service.GastoService;
import br.com.santander.gestaogastos.service.PessoaService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GestaoGastosTests {
	
	@TestConfiguration
    static class GestaoGastosTestContextConfiguration {
		
		@Bean
		public PessoaService pessoaService() {
			return new PessoaService();
		}
  
        @Bean
        public GastoService gastoService() {
            return new GastoService();
        }
    }
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private GastoService gastoService;
	
	private Pessoa p1;
	private Pessoa p2;
	private Pessoa p3;
	private Categoria c1;
	private Categoria c2;
	private Categoria c3;
	private Gasto g1;
	private Gasto g2;
	private Gasto g3;
	private Gasto g4;
	private Gasto g5;
	
	@Before
	public void setUp() throws Exception {
		criarObjetosPessoa();
		criarObjetosCategoria();
		criarObjetosGastoTeste();
		
	}

	@Test
	public void testeGastoServiceSave() throws Exception {
		
		Gasto gastoSave1 = Gasto.builder()
			.descricao("Lanchonete e hamburgueria")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 17, 18, 47, 52))
			.categorias(new HashSet<>())
			.build();
			
		Gasto gastoSave2 = Gasto.builder()
			.descricao("Burguer King")
			.valor(new BigDecimal(22.13))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 1, 19, 19, 2, 10))
			.categorias(new HashSet<>())
			.build();
			
		Gasto gastoSave3 = Gasto.builder()
			.descricao("Loja de Diamantes")
			.valor(new BigDecimal(59999.99))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 2, 2, 9, 26, 22))
			.categorias(new HashSet<>())
			.build();
		
		gastoService.save(gastoSave1);
		gastoService.save(gastoSave2);
		gastoService.save(gastoSave3);
		
		List<Gasto> gastos = selectGastosFromDatabase();
		
		Assert.assertTrue(gastos.contains(g1));
		Assert.assertTrue(gastos.contains(g2));
		Assert.assertTrue(gastos.contains(g3));
		Assert.assertTrue(gastos.size() == 8); // contém os 3 incluídos agora e os 5 já existentes
		
	}

	@Test
	public void testeAdicionarCategoria() throws Exception {
		gastoService.adicionarCategoria(p2, g1.getId(), c1.getId());
		gastoService.adicionarCategoria(p2, g2.getId(), c1.getId());
		
		List<Gasto> gastos = selectGastosFromDatabase();
		
		Set<Gasto> gastosSemCategoria = gastos.stream().filter(gasto -> gasto.getCategorias().isEmpty()).collect(Collectors.toSet());
		Set<Gasto> gastosComCategoria = gastos.stream().filter(gasto -> !gasto.getCategorias().isEmpty()).collect(Collectors.toSet());
		
		Assert.assertTrue(gastosComCategoria.size() == 2);
		Assert.assertTrue(gastosSemCategoria.size() == 3);
		
		
	}
	
	@Test
	public void testeAdicionarCategoriaAutomaticamente() throws Exception {
		gastoService.adicionarCategoria(p2, g1.getId(), c1.getId());
		gastoService.adicionarCategoria(p2, g2.getId(), c1.getId());
		
		Gasto gastoSave1 = Gasto.builder()
			.descricao("Lanchonete e hamburgueria")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 17, 18, 47, 52))
			.categorias(new HashSet<>())
			.build();
		
		Gasto gastoSave2 = Gasto.builder()
			.descricao("Burguer King")
			.valor(new BigDecimal(22.13))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 1, 19, 19, 2, 10))
			.categorias(new HashSet<>())
			.build();
		
		gastoService.save(gastoSave1);
		gastoService.save(gastoSave2);
		
		List<Gasto> gastos = selectGastosFromDatabase();
		
		Set<Gasto> gastosSemCategoria = gastos.stream().filter(gasto -> gasto.getCategorias().isEmpty()).collect(Collectors.toSet());
		Set<Gasto> gastosComCategoria = gastos.stream().filter(gasto -> !gasto.getCategorias().isEmpty()).collect(Collectors.toSet());
		Set<Gasto> gastosDescricao1 = gastosComCategoria.stream().filter(gasto -> gasto.getDescricao().equals("Lanchonete e hamburgueria")).collect(Collectors.toSet());
		Set<Gasto> gastosDescricao2 = gastosComCategoria.stream().filter(gasto -> gasto.getDescricao().equals("Burguer King")).collect(Collectors.toSet());
		
		Assert.assertTrue(gastosSemCategoria.size() == 3);
		Assert.assertTrue(gastosComCategoria.size() == 4);
		Assert.assertTrue(gastosDescricao1.size() == 2);
		Assert.assertTrue(gastosDescricao2.size() == 2);
		
	}
	
	private List<Gasto> selectGastosFromDatabase() {
		List<Gasto> gastos = em.getEntityManager().createQuery("SELECT g FROM Gasto g", Gasto.class).getResultList();
		return gastos;
	}
	
	private void criarObjetosPessoa() {
		p1 = Pessoa.builder()
			.login("SISTEMA")
			.nome("Usuário Sistema")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.SISTEMA)
			.build();
		
		p2 = Pessoa.builder()
			.login("CLIENTE")
			.nome("Usuário Cliente")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.CLIENTE)
			.build();

		p3 = Pessoa.builder()
			.login("CLIENTE 2")
			.nome("Usuário Cliente 2")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.CLIENTE)
			.build();
		
		
		em.persist(p1);
		em.persist(p2);
		em.persist(p3);
		
		em.flush();
		
	}

	private void criarObjetosCategoria() {
		c1 = Categoria.builder()
			.nome("Bares e restaurantes")
			.gastos(new HashSet<>())
			.build();
		
		c2 = Categoria.builder()
			.nome("Mercado")
			.gastos(new HashSet<>())
			.build();
		
		c3 = Categoria.builder()
			.nome("Jóias")
			.gastos(new HashSet<>())
			.build();
		
		em.persist(c1);
		em.persist(c2);
		em.persist(c3);
		
		em.flush();
		
	}

	private void criarObjetosGastoTeste() {
		g1 = Gasto.builder()
			.descricao("Lanchonete e hamburgueria")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 17, 18, 47, 52))
			.categorias(new HashSet<>())
			.build();
		
		g2 = Gasto.builder()
			.descricao("Burguer King")
			.valor(new BigDecimal(22.13))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 1, 19, 19, 2, 10))
			.categorias(new HashSet<>())
			.build();
		
		g3 = Gasto.builder()
			.descricao("Loja de Diamantes")
			.valor(new BigDecimal(59999.99))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 2, 2, 9, 26, 22))
			.categorias(new HashSet<>())
			.build();
		
		g4 = Gasto.builder()
			.descricao("Padaria do Pedro")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 16, 18, 7, 1))
			.categorias(new HashSet<>())
			.build();
		
		g5 = Gasto.builder()
			.descricao("Hipermercado Extra")
			.valor(new BigDecimal(78.21))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 16, 16, 15, 14))
			.categorias(new HashSet<>())
			.build();
		
		g1.addCategoria(c1);
		g2.addCategoria(c1);
		
		em.persist(g1);
		em.persist(g2);
		em.persist(g3);
		em.persist(g4);
		em.persist(g5);
		
		em.flush();
	}
	
}
