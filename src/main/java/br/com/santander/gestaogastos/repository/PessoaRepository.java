package br.com.santander.gestaogastos.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.santander.gestaogastos.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

	Optional<Pessoa> findByLoginAndSenha(String login, String senha);
	
	@Query("SELECT p FROM Pessoa p WHERE p.senha = :senha AND p.login = :login AND p.tipo = 'CLIENTE'")
	Optional<Pessoa> findClienteByLoginAndSenha(String login, String senha);

	@Query("SELECT p FROM Pessoa p WHERE p.id = :id AND p.tipo = 'CLIENTE'")
	Optional<Pessoa> findClienteById(Long id);
}
