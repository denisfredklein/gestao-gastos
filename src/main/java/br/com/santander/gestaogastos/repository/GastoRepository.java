package br.com.santander.gestaogastos.repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.santander.gestaogastos.model.Gasto;

public interface GastoRepository extends CrudRepository<Gasto, Long> {

	@Query("SELECT g FROM Gasto g INNER JOIN g.cliente c WHERE c.id = :id")
	Optional<Set<Gasto>> findGastosByPessoa(Long id);

	@Query("SELECT g FROM Gasto g INNER JOIN g.cliente c WHERE c.id = :id AND g.data BETWEEN :dataInicial AND :dataFinal")
	Optional<Set<Gasto>> findGastosByPessoaAndData(Long id, LocalDateTime dataInicial, LocalDateTime dataFinal);

	Optional<Set<Gasto>> findGastoByDescricao(String descricao);

}
