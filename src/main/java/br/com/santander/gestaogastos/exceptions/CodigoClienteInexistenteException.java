package br.com.santander.gestaogastos.exceptions;

public class CodigoClienteInexistenteException extends Exception {

	private static final long serialVersionUID = -3367358007802444508L;
	
	public CodigoClienteInexistenteException(String msg) {
		super(msg);
	}

}
