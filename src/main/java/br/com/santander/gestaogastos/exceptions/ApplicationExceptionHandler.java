package br.com.santander.gestaogastos.exceptions;

import java.time.DateTimeException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApplicationExceptionHandler {

	@ExceptionHandler
	public ResponseEntity<String> handleException(Exception e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro de servidor: " + e.getMessage());
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(ResultadoNaoEncontradoException e) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(CodigoClienteInexistenteException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(DateTimeException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Formato de data inválido");
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(ConstraintViolationException e) {
		Set<String> msgsErro = e.getConstraintViolations().stream()
				.map(exception -> exception.getMessage())
				.collect(Collectors.toSet());
		
		return ResponseEntity
				.status(HttpStatus.BAD_REQUEST)
				.body("Foram enviados dados inválidos:\n" + msgsErro);
	}
}
