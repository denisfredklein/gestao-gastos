package br.com.santander.gestaogastos.exceptions;

public class ResultadoNaoEncontradoException extends Exception {

	private static final long serialVersionUID = -776413739955279281L;
	
	public ResultadoNaoEncontradoException(String msg) {
		super(msg);
	}

}
