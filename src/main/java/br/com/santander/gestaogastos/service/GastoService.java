package br.com.santander.gestaogastos.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.santander.gestaogastos.exceptions.CodigoClienteInexistenteException;
import br.com.santander.gestaogastos.exceptions.ResultadoNaoEncontradoException;
import br.com.santander.gestaogastos.model.Categoria;
import br.com.santander.gestaogastos.model.Gasto;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.model.TipoPessoaEnum;
import br.com.santander.gestaogastos.repository.CategoriaRepository;
import br.com.santander.gestaogastos.repository.GastoRepository;
import br.com.santander.gestaogastos.repository.PessoaRepository;
import br.com.santander.gestaogastos.utils.SenhaUtils;

@Service
public class GastoService {

	@Autowired
	private GastoRepository gastoRepo;
	
	@Autowired
	private PessoaService pessoaRepo;
	
	@Autowired
	private CategoriaRepository categoriaRepo;
	
	public Gasto save(Gasto gasto) throws Exception {
		Pessoa cliente = pessoaRepo.findById(gasto.getCodigoUsuario());
			
		gasto.setCliente(cliente);
		categorizarAutomaticamente(gasto);
			
		return gastoRepo.save(gasto);
			
	}

	public Gasto adicionarCategoria(Pessoa pessoa, Long idGasto, Long idCategoria) throws Exception {
		pessoaRepo.findClienteByLoginAndSenha(pessoa);
		
		Gasto gasto = gastoRepo
			.findById(idGasto)
			.orElseThrow(() -> new ResultadoNaoEncontradoException("Gasto de ID " + idGasto + " não encontrado."));
		
		Categoria categoria = categoriaRepo
			.findById(idCategoria)
			.orElseThrow(() -> new ResultadoNaoEncontradoException("Categoria de ID " + idCategoria + " não encontrado."));
		
		categoria.getGastos().add(gasto);
		gasto.getCategorias().add(categoria);
		return gastoRepo.save(gasto);
		
	}

	public Set<Gasto> findGastosByPessoa(Pessoa pessoa, LocalDate data) throws Exception {
		pessoa = pessoaRepo.findClienteByLoginAndSenha(pessoa);
		
		Set<Gasto> gastos;
		
		ResultadoNaoEncontradoException rne = new ResultadoNaoEncontradoException("Não foram encontrados gastos com os parâmetros informados");
		
		if(data == null) {
			gastos = gastoRepo
					.findGastosByPessoa(pessoa.getId())
					.orElseThrow(() -> rne);
			
		} else {
			LocalDateTime dataInicial = data.atStartOfDay();
			LocalDateTime dataFinal = data.atStartOfDay().plusDays(1);
			
			gastos = gastoRepo
					.findGastosByPessoaAndData(pessoa.getId(), dataInicial, dataFinal)
					.orElseThrow(() -> rne);
			
		}
		
		return gastos;
		
	}

	public Set<Gasto> findGastosByPessoa(Pessoa pessoa) throws Exception {
		return findGastosByPessoa(pessoa, null);
	}

	private Gasto categorizarAutomaticamente(Gasto gasto) {
		Set<Gasto> gastosDescricaoIgual = gastoRepo
				.findGastoByDescricao(gasto.getDescricao())
				.orElse(new HashSet<>());
		
		gastosDescricaoIgual.stream()
			.filter(gastoDescIgual -> !gastoDescIgual.equals(gasto) 
					&& !gastoDescIgual.getCategorias().isEmpty())
			.map(Gasto::getCategorias)
			.forEach(categorias -> { 
				gasto.getCategorias().addAll(categorias);
				categorias.stream().forEach(cat -> cat.getGastos().add(gasto));
			});
		
		return gasto;
		
	}
	
}
