package br.com.santander.gestaogastos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.santander.gestaogastos.exceptions.CodigoClienteInexistenteException;
import br.com.santander.gestaogastos.exceptions.ResultadoNaoEncontradoException;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.repository.PessoaRepository;
import br.com.santander.gestaogastos.utils.SenhaUtils;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepo;

	public Pessoa findByLoginAndSenha(Pessoa pessoa) throws Exception {
		return pessoaRepo
				.findByLoginAndSenha(pessoa.getLogin(), SenhaUtils.convertToMd5(pessoa.getSenha()))
				.orElseThrow(() -> new ResultadoNaoEncontradoException("Não foi encontrado usuário com este login e senha"));
	}

	public Pessoa findById(Long codPessoa) throws CodigoClienteInexistenteException {
		return pessoaRepo.findClienteById(codPessoa)
				.orElseThrow(() -> new CodigoClienteInexistenteException("O código de cliente informado não existe"));
	}

	public Pessoa findClienteByLoginAndSenha(Pessoa pessoa) throws CodigoClienteInexistenteException {
		return pessoaRepo
				.findClienteByLoginAndSenha(pessoa.getLogin(), SenhaUtils.convertToMd5(pessoa.getSenha()))
				.orElseThrow(() -> new CodigoClienteInexistenteException("O código de cliente informado não existe"));

	}

}
