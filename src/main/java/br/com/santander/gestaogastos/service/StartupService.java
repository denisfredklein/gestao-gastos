package br.com.santander.gestaogastos.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.santander.gestaogastos.model.Categoria;
import br.com.santander.gestaogastos.model.Gasto;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.model.TipoPessoaEnum;
import br.com.santander.gestaogastos.repository.CategoriaRepository;
import br.com.santander.gestaogastos.repository.GastoRepository;
import br.com.santander.gestaogastos.repository.PessoaRepository;

@Service
public class StartupService {
	
	@Autowired
	private GastoRepository gastoRepo;
	
	@Autowired
	private PessoaRepository pessoaRepo;
	
	@Autowired
	private CategoriaRepository categoriaRepo;
	
	private Pessoa p1;
	private Pessoa p2;
	private Pessoa p3;
	private Categoria c1;
	private Categoria c2;
	private Categoria c3;
	private Gasto g1;
	private Gasto g2;
	private Gasto g3;
	private Gasto g4;
	private Gasto g5;
	
	@PostConstruct
	public void init() {
		criarObjetosPessoa();
		criarObjetosCategoria();
		criarObjetosGastoTeste();
		
		pessoaRepo.save(p1);
		pessoaRepo.save(p2);
		pessoaRepo.save(p3);
		
		categoriaRepo.save(c1);
		categoriaRepo.save(c2);
		categoriaRepo.save(c3);
		
		gastoRepo.save(g1);
		gastoRepo.save(g2);
		gastoRepo.save(g3);
		gastoRepo.save(g4);
		gastoRepo.save(g5);
	}
	
	private void criarObjetosPessoa() {
		p1 = Pessoa.builder()
			.login("SISTEMA")
			.nome("Usuário Sistema")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.SISTEMA)
			.build();
		
		p2 = Pessoa.builder()
			.login("CLIENTE")
			.nome("Usuário Cliente")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.CLIENTE)
			.build();

		p3 = Pessoa.builder()
			.login("CLIENTE 2")
			.nome("Usuário Cliente 2")
			.senha("202cb962ac59075b964b07152d234b70")
			.tipo(TipoPessoaEnum.CLIENTE)
			.build();
		
	}

	private void criarObjetosCategoria() {
		c1 = Categoria.builder()
			.nome("Bares e restaurantes")
			.gastos(new HashSet<>())
			.build();
		
		c2 = Categoria.builder()
			.nome("Mercado")
			.gastos(new HashSet<>())
			.build();
		
		c3 = Categoria.builder()
			.nome("Jóias")
			.gastos(new HashSet<>())
			.build();
		
	}

	private void criarObjetosGastoTeste() {
		g1 = Gasto.builder()
			.descricao("Lanchonete e hamburgueria")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 17, 18, 47, 52))
			.categorias(new HashSet<>())
			.build();
		
		g2 = Gasto.builder()
			.descricao("Burguer King")
			.valor(new BigDecimal(22.13))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 1, 19, 19, 2, 10))
			.categorias(new HashSet<>())
			.build();
		
		g3 = Gasto.builder()
			.descricao("Loja de Diamantes")
			.valor(new BigDecimal(59999.99))
			.codigoUsuario(p3.getId())
			.data(LocalDateTime.of(2019, 2, 2, 9, 26, 22))
			.categorias(new HashSet<>())
			.build();
		
		g4 = Gasto.builder()
			.descricao("Padaria do Pedro")
			.valor(new BigDecimal(25.8))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 16, 18, 7, 1))
			.categorias(new HashSet<>())
			.build();
		
		g5 = Gasto.builder()
			.descricao("Hipermercado Extra")
			.valor(new BigDecimal(78.21))
			.codigoUsuario(p2.getId())
			.data(LocalDateTime.of(2019, 1, 16, 16, 15, 14))
			.categorias(new HashSet<>())
			.build();
		
		g1.addCategoria(c1);
		g2.addCategoria(c1);
		
	}
}
