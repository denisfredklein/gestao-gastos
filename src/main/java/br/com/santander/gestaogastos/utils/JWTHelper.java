package br.com.santander.gestaogastos.utils;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JWTHelper {
	private static final String KEY = "TOKEN";

	public String create(String issuer) {
		final Instant now = Instant.now();
		final Date expiryDate = Date.from(now.plus(5, ChronoUnit.MINUTES));
		final Date createDate = Date.from(now);

		Algorithm algorithm = Algorithm.HMAC256(KEY);
		String token = JWT.create()
				.withIssuer(issuer)
				.withNotBefore(createDate)
				.withIssuedAt(createDate)
				.withExpiresAt(expiryDate)
				.sign(algorithm);

		return token;

	}

	public DecodedJWT parseToken(final String token) throws JWTDecodeException, JWTVerificationException,
			IllegalArgumentException, UnsupportedEncodingException {

		Algorithm algorithm = Algorithm.HMAC256(KEY);
		DecodedJWT jwt = JWT.decode(token);

		JWTVerifier verifier = JWT.require(algorithm).build();

		jwt = verifier.verify(token);

		return jwt;

	}
}
