package br.com.santander.gestaogastos.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SenhaUtils {

	private SenhaUtils() {
		
	}
	
	public static String convertToMd5(String senha) {
		
		if (isValidMD5(senha)) {
			return senha;
		}

		MessageDigest mDigest;
		try {
			mDigest = MessageDigest.getInstance("MD5");
			byte[] valorMD5 = mDigest.digest(senha.getBytes("UTF-8"));

			StringBuffer sb = new StringBuffer();
			for (byte b : valorMD5) {
				sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
			}

			return sb.toString();

		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			return null;
		}
	}
	
	public static boolean isValidMD5(String s) {
		return s.matches("[a-fA-F0-9]{32}");
	}
}
