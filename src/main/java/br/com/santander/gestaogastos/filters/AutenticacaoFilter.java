package br.com.santander.gestaogastos.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import br.com.santander.gestaogastos.utils.JWTHelper;

@Component
public class AutenticacaoFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(request, response);
//		HttpServletRequest req = (HttpServletRequest) request;
//		HttpServletResponse res = (HttpServletResponse) response;
//
//		if (req.getRequestURI().endsWith("/gestaogastos/autenticar") || req.getRequestURI().endsWith("/gestaogastos/h2")) {
//			chain.doFilter(request, response);
//			return;
//		}
//		
//		String token = req.getHeader("Authorization");
//
//        if(token == null || token.trim().isEmpty()){
//            res.setStatus(401);
//            return;
//        }
//
//        try {
//            new JWTHelper().parseToken(token);
//            chain.doFilter(request, response);
//            
//        } catch (Exception e) {
//            res.setStatus(401);
//        }
	}

	@Override
	public void init(FilterConfig filterconfig) throws ServletException {

	}
}
