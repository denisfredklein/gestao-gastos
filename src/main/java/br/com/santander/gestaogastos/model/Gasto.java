package br.com.santander.gestaogastos.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="GASTO")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false, of="id")
public class Gasto {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	@Column(name="DS_DESCRICAO")
	@NotBlank(message="O campo descrição é de preenchimento obrigatório")
	@JsonProperty("descricao")
	private String descricao;

	@Column(name="NM_VALOR")
	@NotNull(message="O campo valor é de preenchimento obrigatório")
	@Min(value=0, message="O valor de um lançamento não pode ser menor que zero.")
	@JsonProperty("valor")
	private BigDecimal valor;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PESSOA")
	@JsonIgnore
	private Pessoa cliente;

	@NotNull(message="O campo data é de preenchimento obrigatório")
	@Column(name="DT_LANCAMENTO")
	@JsonProperty("data")
	private LocalDateTime data;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="GASTO_CATEGORIA",
		joinColumns=@JoinColumn(name="ID_GASTO", referencedColumnName="ID"),
		inverseJoinColumns=@JoinColumn(name="ID_CATEGORIA", referencedColumnName="ID")
	)
	private Set<Categoria> categorias = new HashSet<>();
	
	@JsonProperty("codigousuario")
	@Column(name="ID_PESSOA", insertable=false, updatable=false)
	private Long codigoUsuario;
	
	public void addCategoria(Categoria categoria) {
		categorias.add(categoria);
		categoria.getGastos().add(this);
	}
	
}
