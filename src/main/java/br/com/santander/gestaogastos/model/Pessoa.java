package br.com.santander.gestaogastos.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.UniqueElements;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="PESSOA")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false, of="id")
public class Pessoa {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name="ID")
	private Long id;
	
	@Column(name="DS_NOME")
	private String nome;
	
	@Column(name="DS_LOGIN", nullable=false)
	private String login;
	
	@Column(name="DS_SENHA")
	private String senha;
	
	@Enumerated(EnumType.STRING)
	@Column(name="TP_PESSOA")
	private TipoPessoaEnum tipo;
	
	// As especificações não falam que deve ser armazenado o sistema que fez o cadastro.
//	@OneToMany(mappedBy="sistema", cascade={CascadeType.ALL}, orphanRemoval=false)
//	private Set<Gasto> listGastos;
	
}
