package br.com.santander.gestaogastos.controller;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.gestaogastos.exceptions.ResultadoNaoEncontradoException;
import br.com.santander.gestaogastos.model.Gasto;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.service.GastoService;

@RestController
@RequestMapping("/gasto")
public class GastoController {
	
	@Autowired
	private GastoService gastoService;

	@RequestMapping(value="/add",
			method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Gasto> add(@RequestBody Gasto gasto) throws Exception {
			
			gastoService.save(gasto);
			return new ResponseEntity<>(gasto, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/listar",
			method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<Gasto>> add(@RequestBody Pessoa pessoa) throws Exception {
		
		Set<Gasto> gastos = gastoService.findGastosByPessoa(pessoa);
		return new ResponseEntity<>(gastos, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/categoria/{idGasto}/{idCategoria}",
			method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Gasto> addCategoria(@RequestBody Pessoa pessoa,
			@PathVariable Long idGasto, 
			@PathVariable Long idCategoria) throws Exception {
		
		return new ResponseEntity<>(gastoService.adicionarCategoria(pessoa, idGasto, idCategoria), 
			HttpStatus.OK);
		
	}
			
			
	@RequestMapping(value="/listar/{ano}/{mes}/{dia}",
			method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<Gasto>> add(@RequestBody Pessoa pessoa, 
			@PathVariable Integer ano, 
			@PathVariable Integer mes, 
			@PathVariable Integer dia) throws Exception {
		
		LocalDate data = LocalDate.of(ano, mes, dia);
		
		return new ResponseEntity<>(gastoService.findGastosByPessoa(pessoa, data), 
			HttpStatus.OK);
		
	}
	
}
