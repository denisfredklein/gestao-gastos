package br.com.santander.gestaogastos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.gestaogastos.exceptions.ResultadoNaoEncontradoException;
import br.com.santander.gestaogastos.model.Pessoa;
import br.com.santander.gestaogastos.repository.PessoaRepository;
import br.com.santander.gestaogastos.service.PessoaService;
import br.com.santander.gestaogastos.utils.JWTHelper;
import br.com.santander.gestaogastos.utils.SenhaUtils;

@RestController
public class AutenticacaoController {
	
	@Autowired
	private PessoaService pessoaService;

	@RequestMapping(value = "/autenticar", 
			method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> add(@RequestBody Pessoa pessoa) throws Exception {

		pessoa = pessoaService.findByLoginAndSenha(pessoa);
		
		if (pessoa != null) {
			String token = new JWTHelper().create(pessoa.getLogin());
			
			return new ResponseEntity<String>("token: " + token, HttpStatus.OK);
			
		} else {
			throw new ResultadoNaoEncontradoException("Usuário e/ou senha incorretos");
			
		}

	}
}
